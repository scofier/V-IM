package com.v.im.task;

import com.v.im.task.TaskVo;

import java.util.List;

/**
 * @author sk
 */
public interface TaskService {

    boolean saveTask(TaskVo task);

    List<TaskVo> getTask(TaskVo task);

}
