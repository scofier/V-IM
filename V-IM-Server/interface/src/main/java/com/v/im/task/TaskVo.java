package com.v.im.task;


import lombok.Data;
import java.util.Date;

@Data
public class TaskVo {

    private static final long serialVersionUID = 1L;

    private String pid;

    private String name;

    private String describe;

    private Date planStartTime;

    private Date planEndTime;

    private Date startTime;

    private Date endTime;

    private String readStatus;

    private String priorityStatus;

    private String createUid;

    private String executeUid;

    private Integer planCostTime;

    private Integer costTime;

    private String taskType;

}
