package com.v.im.db;

import cn.hutool.core.util.IdUtil;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;


/**
 * @author sk
 */
@Data
public class BaseEntity implements BaseMapper.Interceptor, Serializable {

    @Id
    @Column(nullable = false, length = 32)
    String id;

    private Date createDate;

    private String createBy;

    private Date updateDate;

    private String updateBy;

    private String delFlag;

    @Override
    public void prePersist(){
        if(id == null || id.trim().length() == 0) {
            id = IdUtil.objectId();
        }
    }



}
