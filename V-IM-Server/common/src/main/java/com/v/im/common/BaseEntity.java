package com.v.im.common;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

/**
 * 基础类别
 *
 * @author 乐天
 * @since 2018-10-07
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseEntity<T> {

    private Date createDate;

    private String createBy;

    private Date updateDate;

    private String updateBy;

    @TableLogic(value="0",delval="1")
    private String delFlag;

    public void preInsert() {
        this.createDate = new Date();
        this.updateDate = new Date();
        this.delFlag = "0";
    }

}
