package com.v.im.db;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 通用接口
 * @author sk
 */
public interface CommonWebApi {

    String getEntityPackage();

    /**
     * 返回某个表的数据
     * @param table table
     * @param id ids
     * @return obj
     * @throws ClassNotFoundException ex
     */
    @RequestMapping(value = "/get/{table}", method = {RequestMethod.GET, RequestMethod.POST})
    default Object get(@PathVariable("table") String table, @RequestParam String[] id) throws ClassNotFoundException {
        BaseMapper<?> baseMapper = Dal.with(Class.forName(getEntityPackage() + table));
        if(null == id || id.length ==0 ){
            return baseMapper.selectAll(null);
        }
        return baseMapper.selectByIds(id);
    }

    /**
     * 返回对象列表
     * @param table table
     * @param obj obj
     * @return obj
     * @throws ClassNotFoundException ex
     */
    @RequestMapping(value = "/list/{table}", method = {RequestMethod.GET, RequestMethod.POST})
    default Object list(@PathVariable("table") String table, @RequestParam Map<String, Object> obj) throws ClassNotFoundException {
        Class type = Class.forName(getEntityPackage() + table);
        //移除空类型
        if(null != obj) {
            obj.keySet().removeIf(s -> StrUtil.isEmptyIfStr(obj.get(s)));
        }
        Object criteria = BeanUtil.mapToBean(obj, type, true);

        return Dal.with(type).ignoreBlank().select(criteria);
    }

    /**
     * 删除对象数据
     * @param table
     * @param obj
     * @return
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("all")
    @RequestMapping(value="/delete/{table}",method = {RequestMethod.GET, RequestMethod.POST})
    default Object delete(@PathVariable("table") String table, @RequestParam String[] id) throws ClassNotFoundException {
        Class type = Class.forName(getEntityPackage() + table);
        return Dal.with(type).deleteByIds(id);
    }

    /**
     * 保存对象数据
     * @param table
     * @param obj
     * @return
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("all")
    @RequestMapping(value="/save/{table}",method = {RequestMethod.GET, RequestMethod.POST})
    default Object save(@PathVariable("table") String table, @RequestParam Map<String, Object> obj) throws ClassNotFoundException {
        Class type = Class.forName(getEntityPackage() + table);
        Object criteria = BeanUtil.mapToBean(obj, type, true);
        Object id = obj.get("id");
        if(ObjectUtil.isEmpty(id)) {
            Dal.with(type).ignoreBlank().insert(criteria);
        }else{
            Dal.with(type).ignoreBlank().updateById(criteria);
        }

        return criteria;
    }

    @GetMapping("/ping")
    default Object ping(HttpServletRequest request) {
        return "ok";
    }

}
