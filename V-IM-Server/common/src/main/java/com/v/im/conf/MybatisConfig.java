package com.v.im.conf;


import com.v.im.db.AutoEnumTypeHandler;
import com.v.im.db.BaseMapper;
import com.v.im.db.Dal;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ResolvableType;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Objects;

//@MapperScan("com.demo.repository")
@Configuration
public class MybatisConfig {

    @Resource
    SqlSession sqlSession;

    @Resource
    SqlSessionFactory sqlSessionFactory;

    @PostConstruct
    public void init() {
        org.apache.ibatis.session.Configuration configuration = sqlSessionFactory.getConfiguration();
        configuration
                .getTypeHandlerRegistry()
                .setDefaultEnumTypeHandler(AutoEnumTypeHandler.class);

    }

    @Bean
    @Primary
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public <E> BaseMapper<E> simpleBaseMapper(InjectionPoint ip) {
        ResolvableType resolved = ResolvableType.forField(Objects.requireNonNull(ip.getField()));
        Class<E> parameterClass = (Class<E>) resolved.getGeneric(0).resolve();
        return Dal.with(parameterClass, sqlSession);
    }


}
