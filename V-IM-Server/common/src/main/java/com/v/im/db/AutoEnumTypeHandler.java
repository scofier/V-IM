package com.v.im.db;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * https://segmentfault.com/a/1190000010755321
 * @author scofier
 * @param <E>
 */
@SuppressWarnings("unchecked")
public class AutoEnumTypeHandler<E extends Enum<E>> extends BaseTypeHandler<E> {

    private BaseTypeHandler<E> typeHandler = null;

    public interface EnumValue extends Serializable {

        Integer getValue();

        static <T extends EnumValue> T getEnum(int code, T[] values, String notFoundMsg) {
            for (T value : values) {
                if (value.getValue().equals(code)) {
                    return value;
                }
            }
            throw new IllegalArgumentException(notFoundMsg);
        }

    }

    public AutoEnumTypeHandler(Class<E> type) {
        if (type == null) {
            throw new IllegalArgumentException("Type argument cannot be null");
        }
        if(EnumValue.class.isAssignableFrom(type)){
            // 如果实现了 EnumValue 则使用我们自定义的转换器
            typeHandler = new BaseEnumTypeHandler(type);
        }else {
            // 默认转换器 也可换成 EnumOrdinalTypeHandler
            typeHandler = new org.apache.ibatis.type.EnumTypeHandler<>(type);
        }
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, E parameter, JdbcType jdbcType) throws SQLException {
        typeHandler.setNonNullParameter(ps,i, parameter,jdbcType);
    }

    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return (E) typeHandler.getNullableResult(rs,columnName);
    }

    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return (E) typeHandler.getNullableResult(rs,columnIndex);
    }

    @Override
    public E getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return (E) typeHandler.getNullableResult(cs,columnIndex);
    }


    public class BaseEnumTypeHandler<T extends Enum<?> & EnumValue> extends BaseTypeHandler<EnumValue> {

        private Class<T> type;

        public BaseEnumTypeHandler(Class<T> type) {
            if (type == null) {
                throw new IllegalArgumentException("Type argument cannot be null");
            }
            this.type = type;
        }

        @Override
        public void setNonNullParameter(PreparedStatement ps, int i, EnumValue parameter, JdbcType jdbcType)
                throws SQLException {
            ps.setInt(i, parameter.getValue());
        }

        @Override
        public T getNullableResult(ResultSet rs, String columnName) throws SQLException {
            int code = rs.getInt(columnName);
            return rs.wasNull() ? null : valueOf(code);
        }

        @Override
        public T getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
            int code = rs.getInt(columnIndex);
            return rs.wasNull() ? null : valueOf(code);
        }

        @Override
        public T getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
            int code = cs.getInt(columnIndex);
            return cs.wasNull() ? null : valueOf(code);
        }

        private T valueOf(int code){
            try {
                return valueOf(type, code);
            } catch (Exception ex) {
                throw new IllegalArgumentException("Cannot convert " + code + " to " + type.getSimpleName() + " by code value.", ex);
            }
        }

        private <M extends Enum<?> & EnumValue> M valueOf(Class<M> enumClass, int value) {
            M[] enumConstants = enumClass.getEnumConstants();
            for (M e : enumConstants) {
                if (e.getValue() == value) {
                    return e;
                }
            }
            return null;
        }
    }
}