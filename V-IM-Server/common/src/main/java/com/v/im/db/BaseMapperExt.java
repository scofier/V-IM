package com.v.im.db;


enum BaseMapperExt {

    /** 租户模式 */
    TENANT(new BaseMapper.MapperExt() {
        @Override
        public Object invoke(Object criteria, BaseMapper.SQL sql) {
            if(criteria instanceof BaseEntity) {
            }
            return null;
        }
    }),

    /** 分页 */
    PAGE(new BaseMapper.MapperExt(){
        Object param;
        @Override
        public Object invoke(Object criteria, BaseMapper.SQL sql) {
            final Object param = getParam();
            if(null != param) {
                Integer[] param1 = (Integer[]) param;
                sql.OFFSET(param1[0]).LIMIT(param1[1]);
            }
            return null;
        }

        @Override
        public BaseMapper.MapperExt setParam(Object o) {
            this.param = o;
            return this;
        }

        @Override
        public boolean isPre() {
            return false;
        }

        @Override
        public Object getParam() {
            return this.param;
        }
    }),


    ;


    BaseMapper.MapperExt ext;
    BaseMapperExt(BaseMapper.MapperExt e) {
        this.ext = e;
    }
}


