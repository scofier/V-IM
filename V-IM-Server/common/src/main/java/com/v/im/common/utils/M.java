package com.v.im.common.utils;

import com.v.im.db.BaseEntity;
import com.v.im.db.BaseMapper;
import com.v.im.db.Dal;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Stream;

@Slf4j
public class M {



    public static void sleep(long tm) {
        try {
            Thread.sleep(tm);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String buildSql(Class... entity) {
        StringBuilder sb = new StringBuilder();
        Stream.of(entity).forEach(e->{
            sb.append(BaseMapper.Util.tableInfo(e).toSql());
        });
        return sb.toString();
    }

    public static void createTable(Class... entity) {
        Stream.of(entity).forEach(e->{
            Dal.with(BaseEntity.class).executeSql(BaseMapper.Util.tableInfo(e).toSql());
        });
    }


    public static void responseJsonObject(HttpServletResponse resp, String obj) {
        try (PrintWriter writer = resp.getWriter()) {
            resp.setCharacterEncoding("utf-8");
            resp.setContentType("application/json; charset=utf-8");
            
            writer.write(obj);
        }catch(IOException e) {
            log.error("response obj error: {}", obj, e);
        }
    }



}
