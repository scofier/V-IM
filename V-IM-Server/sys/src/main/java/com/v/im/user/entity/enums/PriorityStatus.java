package com.v.im.user.entity.enums;

import com.v.im.db.AutoEnumTypeHandler;

public enum PriorityStatus implements AutoEnumTypeHandler.EnumValue {
    /**
     * 重要且紧急
     */
    IMPORTANT_AND_URGENT(0),
    /**
     * 重要不紧急
     */
    IMPORTANT_NOT_URGENT(1),
    /**
     * 紧急不重要
     */
    NOT_IMPORTANT_BUT_URGENT(2),
    /**
     * 不重要，不紧急
     */
    NOT_IMPORTANT_NOT_URGENT(3);

    private Integer value;
    PriorityStatus(Integer value) {
        this.value  = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
