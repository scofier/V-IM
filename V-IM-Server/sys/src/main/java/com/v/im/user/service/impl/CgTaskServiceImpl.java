package com.v.im.user.service.impl;

import com.v.im.task.TaskService;
import com.v.im.db.BaseMapper;
import com.v.im.task.TaskVo;
import com.v.im.user.convert.TaskConvert;
import com.v.im.user.entity.CgTask;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("CgTaskServiceImpl")
@Qualifier("CgTaskServiceImpl")
public class CgTaskServiceImpl implements TaskService {

    @Resource
    BaseMapper<CgTask> cgTaskBaseMapper;

    TaskConvert convert = TaskConvert.ME;

    @Override
    public boolean saveTask(TaskVo task) {
        return cgTaskBaseMapper.ignoreBlank().insert(convert.convert(task)) > 0;
    }

    @Override
    public List<TaskVo> getTask(TaskVo task) {
        return convert.convert2Vo(cgTaskBaseMapper.select(convert.convert(task)));
    }
}
