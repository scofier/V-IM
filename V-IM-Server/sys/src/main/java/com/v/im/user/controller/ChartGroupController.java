package com.v.im.user.controller;

import com.v.im.common.exception.ResultCodeEnum;
import com.v.im.common.exception.VimException;
import com.v.im.user.UserUtils;
import com.v.im.user.entity.ImChatGroup;
import com.v.im.user.entity.ImChatGroupUser;
import com.v.im.user.entity.ImUser;
import com.v.im.user.service.IImChatGroupService;
import com.v.im.user.service.IImChatGroupUserService;
import com.v.im.user.service.IImUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;


/**
 * 注册
 *
 * @author 乐天
 * @since 2018-10-07
 */
@RestController
@RequestMapping("/api/chart")
public class ChartGroupController {

    @Resource
    @Qualifier(value = "imChatGroupServiceImpl")
    private IImChatGroupService imChatGroupService;

    @Resource
    @Qualifier(value = "imChatGroupUserService")
    private IImChatGroupUserService imChatGroupUserService;

    /**
     * createGroup
     *
     * @param name     用户名
     * @return 结果
     */
    @ResponseBody
    @RequestMapping(value = "createGroup", method = RequestMethod.POST)
    public boolean createGroup(String groupName) {

        ImUser user = UserUtils.getUser();

        ImChatGroup imChatGroup = new ImChatGroup();
        imChatGroup.setName(groupName);
        imChatGroup.setMaster(user.getId());
        imChatGroup.setCreateDate(new Date());

        boolean ret1 = imChatGroupService.save(imChatGroup);


        ImChatGroupUser imChatGroupUser = new ImChatGroupUser();
        imChatGroupUser.setUserId(user.getId());
        imChatGroupUser.setChatGroupId(imChatGroup.getId());
        imChatGroupUser.setCreateDate(new Date());

        boolean ret2= imChatGroupUserService.save(imChatGroupUser);


        return ret1 && ret2;

    }
}
