package com.v.im.user.entity.enums;

import com.v.im.db.AutoEnumTypeHandler;

public enum ValidStatus implements AutoEnumTypeHandler.EnumValue {
    /**
     * 有效
     */
    TRUE(1),
    /**
     * 无效
     */
    FALSE(0);

    private Integer value;

    ValidStatus(Integer value) {
        this.value  = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
