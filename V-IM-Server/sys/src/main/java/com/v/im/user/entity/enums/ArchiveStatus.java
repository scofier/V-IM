package com.v.im.user.entity.enums;

import com.v.im.db.AutoEnumTypeHandler;

public enum ArchiveStatus implements AutoEnumTypeHandler.EnumValue {
    /**
     * 归档
     */
    TRUE(1),
    /**
     * 非归档
     */
    FALSE(0);


    private Integer value;
    ArchiveStatus(Integer value) {
        this.value  = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
