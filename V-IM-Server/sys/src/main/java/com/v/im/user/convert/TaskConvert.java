package com.v.im.user.convert;

import com.v.im.task.TaskVo;
import com.v.im.user.entity.CgTask;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskConvert {
    TaskConvert ME = Mappers.getMapper(TaskConvert.class);

    CgTask convert(TaskVo taskVo);
    List<CgTask> convert(List<TaskVo> taskVo);


    TaskVo convert(CgTask cgTask);
    List<TaskVo> convert2Vo(List<CgTask> cgTask);
}
