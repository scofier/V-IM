package com.v.im.user.entity.enums;

import com.v.im.db.AutoEnumTypeHandler;

public enum ReadStatus implements AutoEnumTypeHandler.EnumValue {
    /**
     * 已读
     */
    TRUE(1),
    /**
     * 未读
     */
    FALSE(0);

    private Integer value;

    ReadStatus(Integer value) {
        this.value  = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
