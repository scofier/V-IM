package com.v.im.user.entity;

import com.v.im.db.BaseEntity;
import com.v.im.user.entity.enums.ArchiveStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 小组任务类型（需求，任务，缺陷）
 *
 * @author 乐天
 * @since 2018-10-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CgTaskGroup extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    private String name;

    private Date planStartTime;

    private Date planEndTime;

    private Date startTime;

    private Date endTime;

    private ArchiveStatus status;

}
