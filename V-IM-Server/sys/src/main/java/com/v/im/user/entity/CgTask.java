package com.v.im.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.v.im.db.BaseEntity;
import com.v.im.user.entity.enums.ArchiveStatus;
import com.v.im.user.entity.enums.PriorityStatus;
import com.v.im.user.entity.enums.ReadStatus;
import com.v.im.user.entity.enums.TaskType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 小组任务类型（需求，任务，缺陷）
 *
 * @author 乐天
 * @since 2018-10-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CgTask extends BaseEntity implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    private String pid;

    private String name;

    private String describe;

    private Date planStartTime;

    private Date planEndTime;

    private Date startTime;

    private Date endTime;

    private ReadStatus readStatus;

    private PriorityStatus priorityStatus;

    private String createUid;

    private String executeUid;

    private Integer planCostTime;

    private Integer costTime;

    private TaskType taskType;

}
