package com.v.im.user.entity.enums;

import com.v.im.db.AutoEnumTypeHandler;

public enum TaskType implements AutoEnumTypeHandler.EnumValue {
    /**
     * 需求
     */
    ISSUE(0),
    /**
     * 任务
     */
    TASK(1),
    /**
     * 缺陷
     */
    BUG(2);

    private Integer value;

    TaskType(Integer value) {
        this.value  = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
