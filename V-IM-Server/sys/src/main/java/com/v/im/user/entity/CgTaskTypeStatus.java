package com.v.im.user.entity;

import com.v.im.db.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 小组任务类型（需求，任务，缺陷）状态
 *
 * @author 乐天
 * @since 2018-10-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CgTaskTypeStatus extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    private String name;

}
