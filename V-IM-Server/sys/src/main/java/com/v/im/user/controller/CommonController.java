package com.v.im.user.controller;

import com.v.im.db.CommonWebApi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/_c")
public class CommonController implements CommonWebApi {

    private static final String PACKAGE_NAME ="com.v.im.user.entity.";

    @Override
    public String getEntityPackage() {
        return PACKAGE_NAME;
    }



}
