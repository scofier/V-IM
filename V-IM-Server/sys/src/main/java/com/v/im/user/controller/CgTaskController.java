package com.v.im.user.controller;

import com.v.im.task.TaskService;
import com.v.im.task.TaskVo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 退出登录
 *
 * @author 乐天
 * @since 2018-10-07
 */
@RestController
@RequestMapping("/cgTask")
public class CgTaskController {

//    @Autowired
//    BaseMapper<CgTask> cgTaskBaseMapper;


    @Resource
    @Qualifier(value = "CgTaskServiceImpl")
    TaskService taskService;

    /**
     * 退出登录功能
     *
     * @return json
     */
//    @GetMapping("/test/1")
//    public List<CgTask> revokeToken(String[] ids) {
//        return cgTaskBaseMapper.selectByIds(ids);
//    }


    @PostMapping("/save")
    public boolean save(@RequestBody TaskVo taskVo) {
        return  taskService.saveTask(taskVo);
    }

    @GetMapping("/getTask")
    public List<TaskVo> getTask(TaskVo taskVo) {
        return taskService.getTask(taskVo);
    }


}
