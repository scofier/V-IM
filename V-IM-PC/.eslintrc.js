module.exports = {
    // plugins: [
    //     'html',
    //     'vue'	
    // ],
    parserOptions: {
        ecmaVersion: 6,
        sourceType: "module",
        parser: 'babel-eslint'
    },
    parser: "vue-eslint-parser",
    rules: {
        
        "vue/no-parsing-error": ["off"],
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
    }
}